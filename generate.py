#!/usr/bin/env python3
"""
Takes gpx files with tracks,
produces a html file visualizing the tracks on a map
and showing some nice graphs.
"""
import argparse
from collections import defaultdict
import itertools
# import concurrent.futures
from pathlib import Path
import json
import os
import re
import shutil
import folium
import geopy.distance
import gpxpy
from jinja2 import Environment, FileSystemLoader
import numpy as np
import pandas as pd


def track_id(track) -> str:
    """Generate a unique id for given track. Serves as a sort key as well."""
    key = track['start_time']
    key = key.replace(':', '_')
    return key

def s2hms(s):
    """ Convert seconds to hours, minutes and seconds """
    m, s = divmod(s, 60)
    h, m = divmod(m, 60)
    return h, m, s


def pairwise(iterable):
    """Introduced in python 3.10"""
    # pairwise('ABCDEFG') --> AB BC CD DE EF FG
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


def parse_gpx(gpx_path):
    with open(gpx_path,encoding='utf-8') as file:
        return parse(gpxpy.parse(file))


def parse(gpx):
    """Load and parse gpx file."""
    track = defaultdict(list)

    track['name'] = gpx.tracks[0].name
    track['description'] = gpx.tracks[0].description

    # Reduce number of points and smooth
    gpx.reduce_points(None, min_distance=10)
    gpx.smooth(vertical=True, horizontal=True)
    gpx.smooth(vertical=True, horizontal=False)

    track['uphill'], track['downhill'] = gpx.get_uphill_downhill()

    track['date'] = None


    total_moving_time = 0
    total_moving_distance = 0
    dfs = []
    distance_offset = 0
    duration_offset = 0
    for trck in gpx.tracks:
        for sgmt in trck.segments:
            moving_time, _, moving_distance, _, _ = sgmt.get_moving_data()
            total_moving_time += moving_time
            total_moving_distance += moving_distance
            points = []
            for point in sgmt.walk(True):
                if track['date'] is None:
                    track['date'] = point.time.date().isoformat()
                    track['start_time'] = point.time.isoformat()
                datapoint = {
                    'time': point.time,
                    'latitude': point.latitude,
                    'longitude': point.longitude,
                    'elevation': point.elevation,
                }
                # Try to add data from extensions, if present. Assuming a single value per track point.
                for ext in point.extensions:
                    for column, extname in (
                        ('hr', '{http://www.garmin.com/xmlschemas/TrackPointExtension/v1}hr'),
                        ('cad', '{http://www.garmin.com/xmlschemas/TrackPointExtension/v1}cad'),
                    ):
                        try:
                            datapoint[column] = float(ext.find(extname).text)
                        except AttributeError:
                            pass
                points.append(datapoint)

            df = pd.DataFrame.from_records(points)
            df = df.interpolate(method='linear')

            # https://www.gpxz.io/blog/gpx-file-to-pandas
            # Add cumulative distance.
            coords = [(p.latitude, p.longitude) for p in df.itertuples()]
            df['distance'] = [0] + [geopy.distance.distance(from_, to).m
                    for from_, to in zip(coords[:-1], coords[1:])]
            df['cumulative_distance'] = df['distance'].cumsum() + distance_offset
            distance_offset = df['cumulative_distance'].iloc[-1]


            # Add timing.
            try:
                df['duration'] = df.time.diff().dt.total_seconds().fillna(0)
                df['cumulative_duration'] = df['duration'].cumsum() + duration_offset
                duration_offset = df['cumulative_duration'].iloc[-1]
            except:
                pass

            dfs.append(df)

    # Detail data
    track['data'] = pd.concat(dfs, ignore_index=True)

    # Track summary
    track['data']['pace_metric'] = pd.Series((track['data']['duration'] / 60) / (track['data']['distance'] / 1000)).bfill()
    track['distance'] = track['data']['cumulative_distance'].iloc[-1]  # m
    track['duration'] = track['data']['cumulative_duration'].iloc[-1]  # s
    if track['distance'] > 0:
        track['pace'] = (track['duration'] / 60) / (track['distance'] / 1000)
    else:
        print('Warning: track' + track['name'] + " seems to be long 0m")
        track['pace'] = 0.0

    return track


def parse_gpx_dir(dir_path):
    """Load and parse gpx files in given directory including subdirectories."""
#    with concurrent.futures.ProcessPoolExecutor(max_workers=15) as executor:
#        futures = [executor.submit(load_gpx, path) for path in sorted(Path(dir_path).glob('**/*.gpx'))]
#        data = pd.concat([f.result() for f in concurrent.futures.as_completed(futures)]).reset_index(drop=True).sort_values(by='time')
    out = [
        parse_gpx(path)
        for path in Path(dir_path).glob('**/*.gpx')
    ]
    return sorted(out, key=lambda x: track_id(x))


def draw_map(tracks, *, line_options = {'color': 'red', 'weight': 3, 'opacity': 0.4}):
    """
    Group tracks by day
    (multiple tracks in a single day are merged together)
    and draw them on the map.
    """
    m = folium.Map(location=[
            pd.concat(t['data'].latitude for t in tracks).median(),
            pd.concat(t['data'].longitude for t in tracks).median()
        ], zoom_start=11)  # , tiles="Stamen Terrain")
    for track in tracks:
        date = track['date']
        length = track['data'].cumulative_distance.max()
        duration = track['data'].cumulative_duration.max()

        folium.PolyLine(
                track['data'][['latitude','longitude']],
                tooltip=f'{date}, {length/1000:.1f} km, {duration / 60 / (length/1000):.1f} min/km',
                **line_options).add_to(m)

    return m


def draw_single_track(track, *,
                      start_marker=folium.Icon(icon='play', color='green'),
                      end_marker=folium.Icon(icon='stop', color='red'),
                      line_options = {'color': 'red', 'weight': 5, 'opacity': 0.4}):
    """
    Draw a map of a single track.
    """

    m = folium.Map(location=[track['data'].latitude.median(), track['data'].longitude.median()], zoom_start=12)

    sw = (track['data'].latitude.min(), track['data'].longitude.min())
    ne = (track['data'].latitude.max(), track['data'].longitude.max())
    m.fit_bounds([sw, ne])

    if start_marker:
        folium.Marker(location=[track['data'].latitude.iloc[0], track['data'].longitude.iloc[0]], icon=start_marker, tooltip="start").add_to(m)
    if end_marker:
        folium.Marker(location=[track['data'].latitude.iloc[-1], track['data'].longitude.iloc[-1]], icon=end_marker, tooltip="end").add_to(m)

    length = track['data'].cumulative_distance.max()
    duration = track['data'].cumulative_duration.max()
    tooltip = f'{track["date"]}, {length/1000:.1f} km, {duration / 60 / (length/1000):.1f} min/km'

    folium.PolyLine(
            track['data'][['latitude','longitude']],
            tooltip=tooltip,
            **line_options).add_to(m)

    return m


def save_minimaps(tracks, output_dir):
    for track in tracks:
        id = track_id(track)
        map_filename = f'{id}.html'
        m = draw_single_track(track)
        m.save(output_dir / map_filename)


def tracks2json(tracks, jsonfilename, *, data_columns=('cumulative_distance', 'elevation', 'pace_metric', 'hr', 'cad')):
    data={}
    for track in tracks:
        idx = track_id(track)
        map_filename = f'{idx}.html'
        data[f'{idx}'] = {
            'date': track['date'],
            'distance': track['distance'],
            'duration': track['duration'],
            'pace': track['pace'],
            'uphill': track['uphill'],
            'map': map_filename,
        }
        data[f'{idx}']['data'] = {}
        for col in data_columns:
            try:
                data[f'{idx}']['data'].update(
                    track['data'][['cumulative_duration', col]].rolling(window=10, on='cumulative_duration', center=True, min_periods=1).mean().bfill().to_dict(orient='list')
                )
            except KeyError:
                pass  # column not in data (i.e. hr or cad)

    with open(jsonfilename, 'w') as outfile:
        outfile.write(json.dumps(data, indent=4))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--data", default="./data", help="Path to a directory with gpx files.")
    parser.add_argument("--output", default="./output", help="Path to a directory where to generate the content.")
    args = parser.parse_args()

    data_dir = Path(args.data)
    tracks = parse_gpx_dir(data_dir)

    out_dir = Path(args.output)
    out_dir.mkdir(parents=True, exist_ok=True)


    draw_map(tracks).save(out_dir / 'map.html')
    save_minimaps(tracks, out_dir)

    tracks2json(tracks, out_dir / 'data.json')

    shutil.copytree(Path('./templates/'), out_dir, dirs_exist_ok=True)
