# Running analytics

For those who like running, maps and graphs.

A tool to create a web page with maps (using [folium](https://python-visualization.github.io/folium/)) with all tracks (in a [gpx](https://en.wikipedia.org/wiki/GPS_Exchange_Format) format) and graphs with statistics (using [plotly.js](https://plotly.com/javascript/)).

## Installation

Requires `python3-venv`

## Usage

1. add the `.gpx` files to a data folder (e.g. `data`)
2. activate Python venv: `. initenv`
3. generate analytics: `./generate.py --data data --output output`
4. enjoy the maps and graphs e.g. by running `python -m http.server --bind 127.0.0.1 --directory output/ 8889` and navigating to `http://127.0.0.1:8889`
