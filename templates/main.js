function normalize_array(arr) {
    normalize = function(val, max, min) {
        return(val - min) / (max - min);
    }
    
    max = Math.max.apply(null, arr)
    min = Math.min.apply(null, arr)
    
    hold_normed_values=[]
    arr.forEach(function(this_num) {
        hold_normed_values.push(normalize(this_num, max, min))
    })
    
    return(hold_normed_values)
}

function date_to_YYYYMMDD(d) {
    return (d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2))
}

function drawScatterChart(data, elementId) {
    var grapharea = document.getElementById(elementId);
    const date = Array.from(Object.values(data), (x) => Date.parse(x.date));
    const norm_date = normalize_array(date);
    const date_str = Array.from(Object.values(date), (x) => date_to_YYYYMMDD(new Date(x)));
    const distances = Array.from(Object.values(data), (x) => parseFloat(x.distance) / 1000);
    const cumulative_distances = distances.map((sum => value => sum += value)(0));
    const pace = Array.from(Object.values(data), (x) => parseFloat(x.pace));

    Plotly.newPlot(grapharea, {
        data: [
            {
                x: pace,
                y: distances,
                mode: 'markers',
                type: 'scatter',
                marker: {
                    color: norm_date,
                },
                hovertemplate: '%{y:.2f}km, %{x:.2f}min/km',
                hovertemplate: '%{x:.2f}min/km, %{y:.2f}km, date:%{text}',
                text: date_str,
            },
        ],
        layout: {
            title: 'Pace envelope',
            yaxis: {
                title: 'distance [km]',
            },
            xaxis: {
                title: 'pace [min/km]',
                range: [10, 0],
                dtick: 5,
            },
        }
    });
}


function drawSummaryChart(data, element_id) {
    var grapharea = document.getElementById(element_id);
    const date = Array.from(Object.values(data), (x) => x.date);
    const distances = Array.from(Object.values(data), (x) => parseFloat(x.distance) / 1000);
    const cumulative_distances = distances.map((sum => value => sum += value)(0));
    const pace = Array.from(Object.values(data), (x) => parseFloat(x.pace));

    Plotly.newPlot(grapharea, {
        data: [
            {
                x: date,
                y: cumulative_distances,
                fill: 'tozeroy',    
                showlegend: false,
            },
            {
                x: date,
                y: distances,
                showlegend: false,
                xaxis: 'x',
                yaxis: 'y2',
                type: 'bar',
            },
            {
                x: date,
                y: pace,
                showlegend: false,
                xaxis: 'x',
                yaxis: 'y3',
                mode: 'markers',
            },
        ],
        layout: {
            grid: {
                rows: 3,
                columns: 1,
                pattern: 'samex'
                },
            title: 'Overall progress',
            yaxis: {
                title: 'cumulative<br>distance [km]',
            },
            yaxis2: {
                title: 'track<br>distance [km]',
            },
            yaxis3: {
                title: 'pace<br>[min/km]',
                range: [10, 0],
                dtick: 5,
            },
        }
    });
}


function fillTracksTable(data) {
    // Get the container element where the table will be inserted
    let container = $("#track_list");

    // Create the table element
    let table = $("<table>").addClass("w3-table-all w3-hoverable");

    // Get the keys (column names) of the first object in the JSON data
    let cols = ['date', 'distance', 'pace', 'duration'];

    // Create the header element
    let thead = $("<thead>");
    let tr = $("<tr>");

    // Loop through the column names and create header cells
    $.each(cols, function (i, item) {
        let th = $("<th>");
        th.text(item);
        tr.append(th);
    });
    thead.append(tr);

    table.append(tr);

    // Loop through the JSON data and create table rows
    let last_track = false;
    $.each(data, function (i, item) {
        // callback function to update track details
        let tr = $("<tr>").click(function () { select_track(item); });

        // Loop through the values and create table cells
        $.each(cols, function (i, col) {
            let td = $("<td>");
            let elem = item[col];
            switch (col) {
                case 'distance':
                    elem = (elem / 1000).toFixed(1) + "km";
                    break;
                case 'duration':
                    elem = new Date(elem * 1000).toISOString().slice(11, 19);
                    break;
                case 'pace':
                    elem = new Date(elem * 60 * 1000).toISOString().slice(14, 19);
                    break;
            }
            td.text(elem);
            tr.append(td);
        });
        table.append(tr);
        last_track = item
    });
    container.append(table);

    if (last_track) {
        select_track(last_track);
    }
}

function select_track(track) {
    const current_url = document.getElementById('minimap_ctr').src.replace(/^.*[\\/]/, '')
    if (current_url !== track["map"]) {
        document.getElementById('minimap_ctr').src = track["map"];

        let p = $("<p>");
        p.text('date: ' + track["date"]);

        $("#track_details").html(
            "<table><tr><th align='left'>date</th><td align='right'>" + track["date"] + "</td></tr> " +
            "<tr><th align='left'>distance</th><td align='right'>" + (track["distance"] / 1000).toFixed(1) + "km</td></tr> " +
            "<tr><th align='left'>duration</th><td align='right'>" + new Date(track["duration"] * 1000).toISOString().slice(11, 19) + "</td></tr> " +
            "<tr><th align='left'>pace</th><td align='right'>" + new Date(track["pace"] * 60000).toISOString().slice(14, 19) + "</td></tr>" +
            "<tr><th align='left'>uphill</th><td align='right'>" + track["uphill"].toFixed(0) + "m</td></tr></table>"
        );
        // track_detail_chart
        var grapharea = document.getElementById("track_detail_chart");
        Plotly.newPlot(grapharea, {
            data: [
                {
                    x: track["data"]['cumulative_distance'],
                    y: track['data']['elevation'],
                    fill: 'tozeroy',    
                    showlegend: false,
                },
                {
                    x: track["data"]['cumulative_distance'],
                    y: track['data']['pace_metric'],
                    xaxis: 'x2',
                    yaxis: 'y2',
                    showlegend: false,
                },
                {
                    x: track["data"]['cumulative_distance'],
                    y: track['data']['hr'],
                    xaxis: 'x3',
                    yaxis: 'y3',
                    showlegend: false,

                },
                {
                    x: track["data"]['cumulative_distance'],
                    y: track['data']['cad'],
                    xaxis: 'x4',
                    yaxis: 'y4',
                    showlegend: false,
                },
            ],
            layout: {
                grid: {
                    rows: 4,
                    columns: 1,
                    pattern: 'independent'
                    },
                title: 'Track analysis',
                xaxis4: {
                    title: 'distance [m]',
                },
                yaxis: {
                    title: 'elevation<br>[m]',
                    range: [
                        100 * Math.floor(
                            Math.min(...track['data']['elevation']) / 100),
                        100 * Math.ceil(
                            Math.max(...track['data']['elevation']) / 100)
                    ],
                    // showline: true,
                    // mirror: 'ticks',
                    // showgrid: true,
                    // gridcolor: '#00bdbd',
                    // gridwidth: 1,
                    // showline: true,
                    // linecolor: '#0303ff',
                    // linewidth: 2
                },
                yaxis2: {
                    title: 'pace<br>[min/km]',
                    range: [10, 0],
                    dtick: 5,
                },
                yaxis3: {
                    title: 'heart<br> rate',
                },
                yaxis4: {
                    title: 'cadence',
                },
            }
        });
    }
}


function main() {
    // Load data from json file and fill tracks table
    fetch('data.json')
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            fillTracksTable(data);
            const num_runs = Object.keys(data).length;
            const sumDistances = obj => Object.values(obj).reduce((a, b) => a + b.distance, 0);
            const total_distance = sumDistances(data);
            const sumDurations = obj => Object.values(obj).reduce((a, b) => a + b.duration, 0);
            var tds = sumDurations(data);
            var tdh = Math.trunc(tds/3600);
            tds = (tds - tdh * 3600);
            var tdm = Math.trunc(tds/60);
            tds = tds -tdm *60;
            const total_duration = "" + tdh + "h " + tdm + "m";
            document.getElementById('summary').innerHTML = "<p>total: " + (total_distance / 1000).toFixed(1) + "km, " + total_duration + ", " + num_runs + " runs</p>"

            drawSummaryChart(data, 'summary_chart');
            drawScatterChart(data, 'scatter_chart');
        })
}

